<?php
namespace Macdoggie\Component\CurrencyConverter;

abstract class CurrencyAbstract
{
    /**
     * @var ISO3Code
     */
    protected $ISO3Code;

    /**
     * @var ISO3Code
     */
    protected $valueBaseISO3Code;

    /**
     * @var float
     */
    protected $value;

    /**
     * @var \DateTime
     */
    protected $dateUpdated;


    /**
     * Currency constructor.
     * @param ISO3Code $ISO3Code
     * @param float $value
     * @param ISO3Code $valueBaseISO3Code
     */
    public function __construct(ISO3Code $ISO3Code, float $value, ISO3Code $valueBaseISO3Code)
    {
        $this->ISO3Code = $ISO3Code;
        $this->valueBaseISO3Code = $valueBaseISO3Code;
        $this->value = $value;
        $this->dateUpdated = new \DateTime();
    }

    /**
     * @return ISO3Code
     */
    public function getISO3Code()
    {
        return $this->ISO3Code;
    }

    /**
     * @param ISO3Code $ISO3Code
     * @return CurrencyAbstract
     */
    public function setISO3Code(ISO3Code $ISO3Code)
    {
        $this->ISO3Code = $ISO3Code;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return CurrencyAbstract
     */
    public function setValue(float $value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param \DateTime $dateUpdated
     * @return CurrencyAbstract
     */
    public function setDateUpdated(\DateTime $dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }

    /**
     * @return ISO3Code
     */
    public function getValueBaseISO3Code()
    {
        return $this->valueBaseISO3Code;
    }

    /**
     * @param ISO3Code $valueBaseISO3Code
     * @return CurrencyAbstract
     */
    public function setValueBaseISO3Code(ISO3Code $valueBaseISO3Code)
    {
        $this->valueBaseISO3Code = $valueBaseISO3Code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->ISO3Code->getName();
    }

}