<?php
namespace Macdoggie\Component\CurrencyConverter;

interface CurrencyProviderInterface
{
    public function getExchangeRates();
}