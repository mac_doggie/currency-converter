<?php
namespace Macdoggie\Component\CurrencyConverter\Lists;

use Macdoggie\Component\CurrencyConverter\Currency;
use Macdoggie\Component\Lists\Exceptions\InvalidDataTypeException;
use Macdoggie\Component\Lists\Exceptions\InvalidDataValueException;
use Macdoggie\Component\Lists\ListAbstract;

abstract class CurrencyListAbstract extends ListAbstract implements CurrencyListInterface
{
    /**
     * @var Currency[]
     */
    protected $items;

    /**
     * @param Currency $item
     * @param int|null $offset
     * @throws InvalidDataTypeException
     */
    public function add(Currency $item, int $offset = null)
    {
        parent::addItem($item, $offset);
    }

    /**
     * @param array $query
     * @param int $offset
     * @return Currency|null
     * @throws InvalidDataValueException
     */
    public function findOneBy(array $query, int $offset = 0)
    {
        return parent::findOneBy($query, $offset);
    }

    /**
     * @param array $query
     * @param int $limit
     * @param int $offset
     * @return Currency[]
     */
    public function findBy($query = null, int $limit = 0, int $offset = 0): array
    {
        return parent::findBy($query, $limit, $offset);
    }
}