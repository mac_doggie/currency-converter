<?php
namespace Macdoggie\Component\CurrencyConverter\Lists;

use Macdoggie\Component\CurrencyConverter\Currency;
use Macdoggie\Component\Lists\ListInterface;

interface CurrencyListInterface extends ListInterface
{
    public function add(Currency $item, int $offset = null);

}