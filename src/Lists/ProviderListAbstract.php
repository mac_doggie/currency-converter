<?php
namespace Macdoggie\Component\CurrencyConverter\Lists;

use Macdoggie\Component\CurrencyConverter\CurrencyProviderAbstract;
use Macdoggie\Component\Lists\Exceptions\InvalidDataTypeException;
use Macdoggie\Component\Lists\Exceptions\InvalidDataValueException;
use Macdoggie\Component\Lists\ListAbstract;

abstract class ProviderListAbstract extends ListAbstract implements ProviderListInterface
{
    /**
     * @var CurrencyProviderAbstract[]
     */
    protected $items;

    /**
     * @param $item
     * @param int|null $offset
     * @throws InvalidDataTypeException
     */
    public function add($item, int $offset = null)
    {
        if (!is_subclass_of($item, 'Macdoggie\Component\CurrencyConverter\CurrencyProviderAbstract')) {
            throw new InvalidDataTypeException("ProviderListAbstract::add expects parameter1 to be subclass of CurrencyProviderAbstract. Found ". get_class($item));
        }
        parent::addItem($item, $offset);
    }
    
    /**
     * @param array $query
     * @param int $offset
     * @return CurrencyProviderAbstract
     * @throws InvalidDataValueException
     */
    public function findOneBy(array $query, int $offset=0) {
        return parent::findOneBy($query, $offset);
    }

    /**
     * @param array $query
     * @return CurrencyProviderAbstract[]
     * @throws InvalidDataValueException
     */
    public function findBy($query = null, int $limit=0, int $offset=0): array
    {
        return parent::findBy($query, $limit, $offset);
    }

}