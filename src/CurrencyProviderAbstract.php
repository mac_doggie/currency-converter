<?php
namespace Macdoggie\Component\CurrencyConverter;

use Macdoggie\Component\CurrencyConverter\Exceptions\ConfigException;
use Macdoggie\Component\CurrencyConverter\Lists\CurrencyList;
use Symfony\Component\Yaml\Yaml;

abstract class CurrencyProviderAbstract implements CurrencyProviderInterface
{
    protected $config = [];

    /**
     * @var CurrencyList
     */
    protected $currencyList;

    protected function loadConfig($providerName)
    {
        $config = Yaml::parse(\file_get_contents('../Resources/config.yml'));
        if (!array_key_exists($providerName, $config['Providers'])) {
            throw new ConfigException("Provider section {$providerName} not found in config.yml");
        }
        $this->config = $config['Providers'][$providerName];
        $this->currencyList = new CurrencyList();
    }

    protected function addExchangeRate(string $baseCurrency, string $targetCurrency, float $value) {
        try {
            $this->getCurrencyList()->add(new Currency(new ISO3Code($targetCurrency), $value, new ISO3Code($baseCurrency)));
        } catch (\Exception $e) {
            // extra codes that are not ISO4217 are specified in CUSTOM.xml
            $this->getCurrencyList()->add(new Currency(new ISO3Code($targetCurrency, "CUSTOM"), $value, new ISO3Code($baseCurrency)));
        }
    }

    /**
     * @return CurrencyList
     */
    public function getCurrencyList()
    {
        return $this->currencyList;
    }


}