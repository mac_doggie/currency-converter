<?php
namespace Macdoggie\Component\CurrencyConverter\Tests;

use Macdoggie\Component\CurrencyConverter\Currency;
use Macdoggie\Component\CurrencyConverter\Providers\CurrencyLayerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\FixerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\OpenExchangeratesProvider;
use Macdoggie\Component\CurrencyConverter\Visitor\CurrencyListVisitor;

require_once "../../vendor/autoload.php";
require_once "../Helpers/getClass.php";

class VisitorTest extends \PHPUnit_Framework_TestCase
{
    public function testCurrencyListVisitor()
    {
        // setup
        $currencyListVisitor = new CurrencyListVisitor();
        $currencyLayerProvider = new CurrencyLayerProvider();
        $fixerProvider = new FixerProvider();
        $APIID = "9d71d71911494bd9a036abd096e33b51";
        $openExchangeratesProvider = new OpenExchangeratesProvider($APIID);

        // execute
        $currencyListVisitor->visit($currencyLayerProvider);
        $currencyListVisitor->visit($fixerProvider);
        $currencyListVisitor->visit($openExchangeratesProvider);
        $euro = $currencyListVisitor->getCurrencyList()->findBy([[':ISO3Code', '$eq', 'EUR']]);

        // verify
        $this->assertEquals(2, count($euro));
        /** @var Currency $euro */
        $euro = reset($euro);
        $this->assertEquals($euro->getISO3code(), "EUR");
    }
}
