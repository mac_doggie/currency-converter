<?php
namespace Macdoggie\Component\CurrencyConverter\Tests;

use Macdoggie\Component\CurrencyConverter\Currency;
use Macdoggie\Component\CurrencyConverter\CurrencyConverter;
use Macdoggie\Component\CurrencyConverter\ISO3Code;

require_once "../../vendor/autoload.php";
require_once "../Helpers/getClass.php";

class StubCurrencyConverter extends CurrencyConverter {
    public function loadCurrencies(string $APIID)
    {
        self::addCurrency(new Currency(new ISO3Code("USD"), 1.13765, new ISO3Code("EUR")));
        self::addCurrency(new Currency(new ISO3Code("GBP"), 0.7898, new ISO3Code("EUR")));
        self::addCurrency(new Currency(new ISO3Code("GBP"), 0.6943, new ISO3Code("USD")));
        self::addCurrency(new Currency(new ISO3Code("HKD"), 8.8227, new ISO3Code("EUR")));
        self::addCurrency(new Currency(new ISO3Code("INR"), 66.1726, new ISO3Code("USD")));
        self::addCurrency(new Currency(new ISO3Code("CNY"), 6.4602, new ISO3Code("USD")));
    }
}

class CurrencyConverterTest extends \PHPUnit_Framework_TestCase
{
    public function testVisitor()
    {
        // setup
        $APIID = "9d71d71911494bd9a036abd096e33b51";
        $converter = new StubCurrencyConverter($APIID);

        // execute
        $value1 = $converter->Convert(10, new ISO3Code("EUR"), new ISO3Code("GBP"));
        $value2 = $converter->Convert(10, new ISO3Code("USD"), new ISO3Code("EUR"));
        $value3 = $converter->Convert(10, new ISO3Code("INR"), new ISO3Code("CNY"));
        $value4 = $converter->Convert(10, new ISO3Code("HKD"), new ISO3Code("USD"));
        $value5 = $converter->Convert(10, new ISO3Code("EUR"), new ISO3Code("USD"));
        $value6 = $converter->Convert(10, new ISO3Code("GBP"), new ISO3Code("USD"));

        // verify
        $this->assertEquals(7.90, round($value1, 2), "10 EUR to GBP conversion");
        $this->assertEquals(8.79, round($value2, 2), "10 USD to EUR conversion");
        $this->assertEquals(0.98, round($value3, 2), "10 INR to CNY conversion");
        $this->assertEquals(1.29, round($value4, 2), "10 HKD to USD conversion");
        $this->assertEquals(11.38, round($value5, 2), "10 EUR to USD conversion");
        $this->assertEquals(14.40, round($value6, 2), "10 USD to GBP conversion");
    }
}
