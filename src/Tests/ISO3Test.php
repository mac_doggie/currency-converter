<?php
namespace Macdoggie\Component\CurrencyConverter\Tests;

use Macdoggie\Component\CurrencyConverter\Currency;
use Macdoggie\Component\CurrencyConverter\CurrencyList;
use Macdoggie\Component\CurrencyConverter\ISO3Code;

require_once "../../vendor/autoload.php";

class ISO3Test extends \PHPUnit_Framework_TestCase
{
    public function testReformatting()
    {
        // setup
        $currency1 = new Currency(new ISO3Code("eur"), 2.03, new ISO3Code("usd"));
        $currency2 = new Currency(new ISO3Code("usdollar"), 1.0234, new ISO3Code("HKD"));

        // execute

        // verify
        $this->assertEquals('EUR', $currency1->getISO3Code(), "currency ISO3code uppercase");
        $this->assertEquals('USD', $currency2->getISO3Code(), "currency ISO3code max. 3 characters");
    }

    public function testInvalidDataValue()
    {
        // setup
        $this->expectException('Macdoggie\Component\CurrencyConverter\Exceptions\InvalidDataValueException');

        // execute
        new Currency(new ISO3Code("123"), 2.03, new ISO3Code("eur"));

        // verify
    }
}
