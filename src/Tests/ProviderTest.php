<?php
namespace Macdoggie\Component\CurrencyConverter\Tests;

use Macdoggie\Component\CurrencyConverter\Providers\CurrencyLayerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\FixerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\OpenExchangeratesProvider;

require_once "../../vendor/autoload.php";

class ProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testProviderCurrencyLayer()
    {
        // setup
        $provider = new CurrencyLayerProvider();

        // execute
        $currencies = $provider->getExchangeRates();

        // verify
        $this->assertNotNull($currencies, "Provider CurrencyLayer returns no currencies");
    }

    public function testProviderOpenExchangeRated()
    {
        // setup
        $APIID = "9d71d71911494bd9a036abd096e33b51";
        $provider = new OpenExchangeratesProvider($APIID);

        // execute
        $currencies = $provider->getExchangeRates();

        // verify
        $this->assertNotNull($currencies, "Provider OpenExchangeRated returns no currencies");
    }

    public function testProviderFixer()
    {
        // setup
        $provider = new FixerProvider();

        // execute
        $currencies = $provider->getExchangeRates();

        // verify
        $this->assertNotNull($currencies, "Provider Fixer returns no currencies");
    }

}
