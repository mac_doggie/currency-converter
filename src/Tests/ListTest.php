<?php
namespace Macdoggie\Component\CurrencyConverter\Tests;

use Macdoggie\Component\CurrencyConverter\Currency;
use Macdoggie\Component\CurrencyConverter\Lists\CurrencyList;
use Macdoggie\Component\CurrencyConverter\ISO3Code;
use Macdoggie\Component\Lists\MixedList;
use Macdoggie\Component\CurrencyConverter\Lists\ProviderList;
use Macdoggie\Component\CurrencyConverter\Providers\CurrencyLayerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\FixerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\OpenExchangeratesProvider;

require_once "../../vendor/autoload.php";

class ListTest extends \PHPUnit_Framework_TestCase
{
    public function testMixedList()
    {
        // setup

        // execute
        $list = new MixedList();
        $list->add("ABC");
        $list->add(1200);
        $list->add([0,1,2]);
        $list->add(true, 1);

        $first = $list->getFirst();
        $second = $list->getNext();
        $third = $list->getNext();

        // verify
        $this->assertEquals($first, "ABC");
        $this->assertEquals(gettype($second), "boolean");
        $this->assertEquals($second, true);
        $this->assertEquals(gettype($third), "integer");
        $this->assertEquals($third, 1200);
    }

    public function testCurrencyList()
    {
        // setup
        $currency1 = new Currency(new ISO3Code("eur"), 2.03, new ISO3Code("usd"));
        $currency2 = new Currency(new ISO3Code("usd"), 1.0234, new ISO3Code("usd"));
        $currency3 = new Currency(new ISO3Code("CNY"), 0.0674, new ISO3Code("usd"));
        $currency4 = new Currency(new ISO3Code("HKD"), 0.044, new ISO3Code("usd"));

        // execute
        $list = new CurrencyList();
        $list->add($currency1);
        $list->add($currency2);
        $list->add($currency3);
        $list->add($currency4);

        $first = $list->getFirst();
        $second = $list->getNext();
        $last = $list->getLast();
        $preLast = $list->getPrevious();

        // verify
        $this->assertEquals($first, $currency1);
        $this->assertEquals($second, $currency2);
        $this->assertEquals($last, $currency4);
        $this->assertEquals($preLast, $currency3);

    }

    public function testProviderList()
    {
        // setup
        $provider1 = new CurrencyLayerProvider();
        $provider2 = new FixerProvider();
        $provider3 = new OpenExchangeratesProvider("9d71d71911494bd9a036abd096e33b51");

        // execute
        $list = new ProviderList();
        $list->add($provider1);
        $list->add($provider2);
        $list->add($provider3);

        // verify
        $this->assertCount(3, $list);

    }

    public function testListQuery()
    {
        // setup
        $currency1 = new Currency(new ISO3Code("eur"), 2.03, new ISO3Code("usd"));
        $currency2 = new Currency(new ISO3Code("usd"), 1.0234, new ISO3Code("usd"));
        $currency3 = new Currency(new ISO3Code("CNY"), 0.0674, new ISO3Code("usd"));
        $currency4 = new Currency(new ISO3Code("HKD"), 0.044, new ISO3Code("usd"));
        $currency5 = new Currency(new ISO3Code("AZN"), 1.652055, new ISO3Code("usd"));
        $currency6 = new Currency(new ISO3Code("AZN"), 1.22255, new ISO3Code("eur"));
        $list = new CurrencyList();
        $list->add($currency1);
        $list->add($currency2);
        $list->add($currency3);
        $list->add($currency4);
        $list->add($currency5);
        $list->add($currency6);

        // execute
        $items1 = $list->findBy([
            [':value', '$lt', 1]
        ]);
        $items2 = $list->findBy([
            [':value', '$gt', 1]
        ]);
        $items3 = $list->findBy([
            [':value', '$gt', 0.06],
            [':value', '$lt', 1]
        ]);
        $items4 = $list->findBy([
            [':value', '$gt', 0.06],
            [ '$or',
                [':ISO3Code', '$eq', 'USD'],
                ['$and',
                    [':valueBaseISO3Code', '$eq', 'EUR'],
                    ['$or',
                        [':ISO3Code', '$eq', 'AZN'],
                        [':ISO3Code', '$eq', 'BHD']
                    ]
                ]
            ]
        ]);

        // verify
        $this->assertCount(2, $items1);
        $this->assertEquals("CNY", $items1[0]->getISO3Code());
        $this->assertEquals("USD", $items1[0]->getValueBaseISO3Code());
        $this->assertCount(4, $items2);
        $this->assertEquals("EUR", $items2[0]->getISO3Code());
        $this->assertEquals("USD", $items2[0]->getValueBaseISO3Code());
        $this->assertCount(1, $items3);
        $this->assertEquals("CNY", $items3[0]->getISO3Code());
        $this->assertCount(2, $items4);
        $this->assertEquals("USD", $items4[0]->getISO3Code());
        $this->assertEquals("AZN", $items4[1]->getISO3Code());
    }
}
