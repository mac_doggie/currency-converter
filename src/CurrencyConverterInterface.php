<?php
namespace Macdoggie\Component\CurrencyConverter;

interface CurrencyConverterInterface
{
    /**
     * @param string $APIID
     * @throws Exceptions\VisitorException
     */
    public function loadCurrencies(string $APIID);

    /**
     * @param ISO3Code $source
     * @param ISO3Code $destination
     * @return float
     */
    public function Convert(float $amount, ISO3Code $source, ISO3Code $destination);
}