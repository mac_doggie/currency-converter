<?php
namespace Macdoggie\Component\CurrencyConverter;

use Macdoggie\Component\CurrencyConverter\Exceptions\FileException;
use Macdoggie\Component\CurrencyConverter\Exceptions\InvalidDataValueException;

class ISO3Code
{
    const ISO_CODES_LOCATION = "../Resources/";

    /**
     * @var array
     */
    protected static $ISO = [];

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $iso;

    public function __construct(string $code, $iso="ISO4217")
    {
        $this->iso=$iso;
        if (!array_key_exists($this->iso, self::$ISO)) {
            self::Initialize();
        }

        $code = strtoupper(substr($code, 0, 3));
        if (!self::isValidISO3Code($code, $this->iso)) {
            throw new InvalidDataValueException("Invalid ISO3Code `{$code}`");
        }

        $this->code = $code;
        $this->setName();
    }

    /**
     * @param string $ISO3Code
     * @return bool
     */
    private static function isValidISO3Code(string $ISO3Code, string $iso="ISO4217")
    {
        $codes = self::$ISO[$iso]->xpath('//Ccy');
        for ($i = 0, $found = false; $i < count($codes) && !$found; $i++) {
            $found = ((string)$codes[$i] == $ISO3Code);
        }
        return $found;
    }

    private function Initialize()
    {
        $file = self::ISO_CODES_LOCATION.$this->iso.".xml";
        if (!file_exists($file) || !is_file($file)) {
            throw new FileException("File not found: ".getcwd()."/" . $file);
        }
        self::$ISO[$this->iso] = \simplexml_load_file($file);
    }

    public function __toString()
    {
        return $this->code;
    }

    private function setName()
    {
        $codes = self::$ISO[$this->iso]->xpath('//CcyNtry');
        for ($i = 0, $this->name = null; $i < count($codes) && $this->name == null; $i++) {
            if (((string)$codes[$i]->Ccy == $this->code)) {
                $this->name = (string)$codes[$i]->CcyNm;
            }
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }


}