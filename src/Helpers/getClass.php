<?php
    namespace Macdoggie\Component\CurrencyConverter\Helpers;

    /**
     * returns namespace and/or class name index == 0 namespace, index == 1 class name, array with both if omitted
     * @param $object
     * @param null $index
     * @return string|string[]
     */
    function getClass($object, $index=null)
    {
        $data = explode('\\', get_class($object));
        $class = array_pop($data);
        $namespace = implode('\\', $data);
        $data = [$namespace, $class];

        return (!empty($index)) ? $data[$index] : $data;
    }