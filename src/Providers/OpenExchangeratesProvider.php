<?php
namespace Macdoggie\Component\CurrencyConverter\Providers;

use GuzzleHttp\Client;
use Macdoggie\Component\CurrencyConverter\CurrencyProviderAbstract;
use Macdoggie\Component\CurrencyConverter\CurrencyProviderInterface;
use Macdoggie\Component\CurrencyConverter\Exceptions\ConfigException;
use Macdoggie\Component\CurrencyConverter\Visitor\VisiteeInterface;
use Macdoggie\Component\CurrencyConverter\Visitor\VisiteeTrait;

class OpenExchangeratesProvider extends CurrencyProviderAbstract implements CurrencyProviderInterface, VisiteeInterface
{
    use VisiteeTrait;
    
    private $jsonData;
    private $APIID;

    public function __construct($APIID)
    {
        $this->APIID = $APIID;
    }

    public function getExchangeRates()
    {
        $this->loadConfig("OpenExchangerates");
        if (!array_key_exists("APIEndpoint", $this->config)) {
            throw new ConfigException("APIEndpoint missing in config.yml");
        }

        $guzzle = new Client();
        $result = $guzzle->request("GET", str_replace("%%APIID%%", $this->APIID, $this->config['APIEndpoint']));
        $this->jsonData = json_decode((string)$result->getBody());
        $this->parseExchangeRates();

        return $this->getCurrencyList();
    }

    private function parseExchangeRates()
    {
        if (!empty($this->jsonData) && is_object($this->jsonData)) {
            $properties = get_object_vars($this->jsonData->rates);
            foreach($properties as $prop => $value) {
                $this->addExchangeRate(
                    $this->jsonData->base,
                    substr($prop, 0, 3),
                    $value
                );
            }
        }
    }

}