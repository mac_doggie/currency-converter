<?php
namespace Macdoggie\Component\CurrencyConverter\Providers;

use GuzzleHttp\Client;
use Macdoggie\Component\CurrencyConverter\CurrencyProviderAbstract;
use Macdoggie\Component\CurrencyConverter\CurrencyProviderInterface;
use Macdoggie\Component\CurrencyConverter\Exceptions\ConfigException;
use Macdoggie\Component\CurrencyConverter\Visitor\VisiteeInterface;
use Macdoggie\Component\CurrencyConverter\Visitor\VisiteeTrait;

require_once "../../vendor/autoload.php";

class CurrencyLayerProvider extends CurrencyProviderAbstract implements CurrencyProviderInterface, VisiteeInterface
{
    use VisiteeTrait;

    private $jsonData;

    public function getExchangeRates()
    {
        $this->loadConfig("CurrencyLayer");
        if (!array_key_exists("APIEndpoint", $this->config)) {
            throw new ConfigException("APIEndpoint missing in config.yml");
        }

        $guzzle = new Client();
        $result = $guzzle->request("GET", $this->config['APIEndpoint']);
        $this->jsonData = json_decode((string)$result->getBody());
        $this->parseExchangeRates();

        return $this->getCurrencyList();
    }

    private function parseExchangeRates()
    {
        if ($this->jsonData->success) {
            $properties = get_object_vars($this->jsonData->quotes);
            foreach($properties as $prop => $value) {
                $this->addExchangeRate(
                    substr($prop, 0, 3),
                    substr($prop, 3, 3),
                    $value
                );
            }
        }
    }
}