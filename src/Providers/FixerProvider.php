<?php
namespace Macdoggie\Component\CurrencyConverter\Providers;

use GuzzleHttp\Client;
use Macdoggie\Component\CurrencyConverter\CurrencyProviderAbstract;
use Macdoggie\Component\CurrencyConverter\CurrencyProviderInterface;
use Macdoggie\Component\CurrencyConverter\Exceptions\ConfigException;
use Macdoggie\Component\CurrencyConverter\Visitor\VisiteeInterface;
use Macdoggie\Component\CurrencyConverter\Visitor\VisiteeTrait;

class FixerProvider extends CurrencyProviderAbstract implements CurrencyProviderInterface, VisiteeInterface
{
    use VisiteeTrait;
    
    public function getExchangeRates()
    {
        $this->loadConfig("Fixer");
        if (!array_key_exists("APIEndpoint", $this->config)) {
            throw new ConfigException("APIEndpoint missing in config.yml");
        }

        $guzzle = new Client();
        $result = $guzzle->request("GET", $this->config['APIEndpoint']);
        $this->jsonData = json_decode((string)$result->getBody());
        $this->parseExchangeRates();

        return $this->getCurrencyList();
    }

    private function parseExchangeRates()
    {
        if (!empty($this->jsonData) && is_object($this->jsonData)) {
            $properties = get_object_vars($this->jsonData->rates);
            foreach($properties as $prop => $value) {
                $this->addExchangeRate(
                    $this->jsonData->base,
                    substr($prop, 0, 3),
                    $value
                );
            }
        }
    }

}