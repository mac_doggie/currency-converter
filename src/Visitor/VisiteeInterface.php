<?php


namespace Macdoggie\Component\CurrencyConverter\Visitor;


use Macdoggie\Component\CurrencyConverter\Exceptions\VisitorException;

interface VisiteeInterface
{
    /**
     * @param VisitorAbstract $visitor
     * @throws VisitorException
     * @return mixed
     */
    public function accept(VisitorAbstract $visitor);
}