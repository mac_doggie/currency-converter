<?php


namespace Macdoggie\Component\CurrencyConverter\Visitor;


use Macdoggie\Component\CurrencyConverter\Exceptions\VisitorException;

interface VisitorInterface
{
    /**
     * @param VisiteeInterface $visitee
     * @throws VisitorException
     * @return void
     */
    public function visit(VisiteeInterface $visitee);

    /**
     * @param VisiteeInterface $visitee
     * @throws VisitorException
     * @return void
     */
    public function visitDefault(VisiteeInterface $visitee);
}