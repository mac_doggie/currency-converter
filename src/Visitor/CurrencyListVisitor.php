<?php


namespace Macdoggie\Component\CurrencyConverter\Visitor;


use Macdoggie\Component\CurrencyConverter\Lists\CurrencyList;
use Macdoggie\Component\CurrencyConverter\Providers\CurrencyLayerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\FixerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\OpenExchangeratesProvider;

class CurrencyListVisitor extends VisitorAbstract
{
    /**
     * @var CurrencyList
     */
    private $currencyList;

    public function __construct()
    {
        $this->currencyList = new CurrencyList();
    }

    public function visitCurrencyLayerProvider(CurrencyLayerProvider $provider)
    {
        $provider->getExchangeRates();
        $list = $provider->getCurrencyList();
        foreach ($list as $currency) {
            $this->currencyList->add($currency);
        }
    }

    public function visitFixerProvider(FixerProvider $provider)
    {
        $provider->getExchangeRates();
        $list = $provider->getCurrencyList();
        foreach ($list as $currency) {
            $this->currencyList->add($currency);
        }
    }

    public function visitOpenExchangeratesProvider(OpenExchangeratesProvider $provider)
    {
        $provider->getExchangeRates();
        $list = $provider->getCurrencyList();
        foreach ($list as $currency) {
            $this->currencyList->add($currency);
        }
    }

    /**
     * @return CurrencyList
     */
    public function getCurrencyList()
    {
        return $this->currencyList;
    }
}