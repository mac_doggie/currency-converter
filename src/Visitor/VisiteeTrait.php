<?php

namespace Macdoggie\Component\CurrencyConverter\Visitor;

use Macdoggie\Component\CurrencyConverter\Exceptions\VisitorException;
use function Macdoggie\Component\CurrencyConverter\Helpers\getClass;

trait VisiteeTrait
{
    public function accept(VisitorAbstract $visitor)
    {
        $visitMethods = get_class_methods($visitor);
        $elementClass = getClass($this, 1);

        for ($i = 0, $found = false; $i < count($visitMethods) && !$found; $i++) {
            $method = $visitMethods[$i];
            if ('visit' . $elementClass == $method) {
                $visitor->{'visit' . $elementClass}($this);
                $found = true;
            }
        }
        if (!$found) {
            throw new VisitorException(get_class($visitor) . '->visit' . $elementClass. " not implemented");
        }
    }
}