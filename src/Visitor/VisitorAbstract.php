<?php

namespace Macdoggie\Component\CurrencyConverter\Visitor;

use Macdoggie\Component\CurrencyConverter\Exceptions\VisitorException;
use function Macdoggie\Component\CurrencyConverter\Helpers\getClass;

abstract class VisitorAbstract implements VisitorInterface
{
    /**
     * @param VisiteeInterface $visitee
     * @throws VisitorException
     * @return void
     */
    public function visit(VisiteeInterface $visitee)
    {
        if(!method_exists($visitee, "Accept")) {
            throw new VisitorException("class ".getClass($visitee, 1)." does not accept visitors");
        }
        $visitee->accept($this);
    }

    /**
     * @param VisiteeInterface $visitee
     * @throws VisitorException
     * @return void
     */
    public function visitDefault(VisiteeInterface $visitee)
    {
        throw new VisitorException(get_called_class()."->visit".getClass($visitee)." is not implemented");
    }


}