<?php
namespace Macdoggie\Component\CurrencyConverter;

use Macdoggie\Component\CurrencyConverter\Lists\CurrencyList;
use Macdoggie\Component\CurrencyConverter\Providers\CurrencyLayerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\FixerProvider;
use Macdoggie\Component\CurrencyConverter\Providers\OpenExchangeratesProvider;
use Macdoggie\Component\CurrencyConverter\Visitor\CurrencyListVisitor;

class CurrencyConverter implements CurrencyConverterInterface
{
    /**
     * @var CurrencyList
     */
    protected static $currencies;

    public function __construct(string $APIID)
    {
        $this->loadCurrencies($APIID);
    }

    public static function Initialize()
    {
        self::$currencies = new CurrencyList();
    }

    public static function addCurrency(Currency $currency)
    {
        if (empty(self::$currencies)) {
            self::Initialize();
        }
        self::$currencies->add($currency);
    }

    /**
     * @inheritdoc
     */
    public function loadCurrencies(string $APIID)
    {
        if (empty(self::$currencies)) {
            $currencyListVisitor = new CurrencyListVisitor();
            $currencyLayerProvider = new CurrencyLayerProvider();
            $fixerProvider = new FixerProvider();
            $openExchangeratesProvider = new OpenExchangeratesProvider($APIID);

            $currencyListVisitor->visit($currencyLayerProvider);
            $currencyListVisitor->visit($fixerProvider);
            $currencyListVisitor->visit($openExchangeratesProvider);

            self::$currencies = $currencyListVisitor->getCurrencyList();
        }
    }

    /**
     * @inheritdoc
     */
    public function Convert(float $amount, ISO3Code $source, ISO3Code $destination)
    {
        $value = null;
        $firstChoice = self::$currencies->findOneBy([
            [':ISO3Code', '$eq', $destination],
            [':valueBaseISO3Code', '$eq', $source]
        ]);

        if (!empty($firstChoice)) {
            $value = $amount * $firstChoice->getValue();
        } else {
            $secondChoice = self::$currencies->findOneBy([
                [':ISO3Code', '$eq', $source],
                [':valueBaseISO3Code', '$eq', $destination]
            ]);

            if (!empty($secondChoice)) {
                $value = $amount / $secondChoice->getValue();
            }
        }

        if (empty($firstChoice) && empty($secondChoice)) {
            $sourceCurrencies = self::$currencies->findBy([[':ISO3Code', '$eq', $source]]);
            $sourceCurrency = null;
            $destinationCurrency = null;
            for ($i = 0, $found = false; $i < count($sourceCurrencies) && !$found; $i++) {
                $sourceCurrency = $sourceCurrencies[$i];
                $destinationCurrency = self::$currencies->findOneBy([
                    [':ISO3Code', '$eq', $destination],
                    [':valueBaseISO3Code', '$eq', $sourceCurrency->getValueBaseISO3Code()]
                ]);
                $found = !empty($destinationCurrency);
            }

            if($found) {
                $factor = $sourceCurrency->getValue();
                $value = $amount * $destinationCurrency->getValue() / $factor;
            }
        }

        return $value;
    }
}