Library: macdoggie/currency-converter
=======================

This package can be used to fetch exchange rates from different suppliers and it supplies a currency converter to 
convert an amount of money from one to another currency

## Installing
run the following line from your project root:

    composer require macdoggie/currency-converter
    
## Providers
This package uses the following providers to collect a list of currency information. It is adviced that you cache the 
currencies and update them regularly

* CurrencyLayer
* Fixer
* OpenExchangerates (this provider requires a free registration key. Please do not use the key included in this 
package as it might get blocked due to heavy usage. visit https://openexchangerates.org/signup/free to get your own key)

    
## Using the converter
The following example shows you how to use the converter, the list of currencies is automatically loaded, the first time 
the Converter is instantiated, this might take a while and therefore it is adviced to build in your own caching system. 
How to do this is described below.

    // setup
    $APIID = "9d71d71011294bd9a036abd096e33b51"; // your free openExchangeRates API key (get it at: https://openexchangerates.org/signup/free)
    $converter = new CurrencyConverter($APIID);
    echo $converter->Convert(10, new ISO3Code("USD"), new ISO3Code("EUR"));
    
## Caching the data
Below you will find an example on how to cache the currencies. Choose your own data store to cache the currencies

    class MyCurrencyConverter extends CurrencyConverter 
    {
        public function loadCurrencies(string $APIID)
        {
            if (!$this->readFromCache())
                parent::loadCurrencies($APIID);
                $this->storeInCache()
            }
        }
        
        private function storeInCache() 
        {
            // ToDo: add code to cache your currencies here, they are stored in self::$currencies
        }
        
        /**
        * @return bool
        */
        private function readFromCache() 
        {
            // ToDo: add code to get your currencies from the cache, they should be added with 
            // $this->addCurrency(Currency $currency);
        }
    }
    

    

